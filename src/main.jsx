import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import './index.css'
import { Provider } from 'react-redux'
import { pizzaStore } from './component/e-pizza/store/pizzaStore'

ReactDOM.createRoot(document.getElementById('root')).render(
  <Provider store={pizzaStore}>
  {/* <React.StrictMode> */}
    <App />
  {/* </React.StrictMode> */}
  </Provider>
  ,
)
