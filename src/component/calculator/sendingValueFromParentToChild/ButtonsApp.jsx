 function ButtonsApp({buttonValue}){

    const buttons = ["C","1","2","3","4","5",,"+","6","7","8","9","0","-","*","="];

return(
    <>
    {buttons.map((buttonName,key)=> (
        <div key={key} className="col-md-2">
            <button onClick={()=>buttonValue(buttonName)}>{buttonName}</button>
        </div>  
    ))}
    </>
    )

}
export default ButtonsApp;