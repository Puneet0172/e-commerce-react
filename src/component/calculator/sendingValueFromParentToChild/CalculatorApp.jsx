import ButtonsApp from "./ButtonsApp";

import DisplayApp from "./DisplayApp";




import { useState } from "react";
function CalculatorApp(){
    const [displayKey, SetDisplayKey] = useState("");
/* 
Get data from Child

*/
    function getKeyValue(buttonValue){
        console.log("value pressed",buttonValue)
      
        if(buttonValue === "="){
            console.log(buttonValue= displayKey+buttonValue)
            SetDisplayKey(eval(displayKey));
        } else if(buttonValue ==="C"){
            SetDisplayKey("")
        } else {
            SetDisplayKey(displayKey+buttonValue);
        }
    }

    return (
        <>
        
        <div className="row container col-md-4 ">
        <DisplayApp calculatedValue={displayKey}></DisplayApp>
        <ButtonsApp buttonValue={getKeyValue}></ButtonsApp>
        </div>
        </>
    )
}

export default CalculatorApp;