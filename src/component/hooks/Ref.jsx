import { useEffect, useRef, useState } from "react"

function Ref (){
/**
 
1. useRef is used to access the dom element
2. Always hold the previous value
3. Holds the multiple re-rended value

*/


const [firstName,SetFirstName]=useState("");
const [counter,SetCounter]=useState(0);
const elementIP = useRef("")// useRef is always contain an object which has current is the key 
const prevElementIP = useRef("")// useRef is always contain an object which has current is the key 
const prevCounter = useRef(0)// useRef is always contain an object which has current is the key 



// similar like #ref in agular to get the value @viewchild('ref')
function changeName(){
    
    SetFirstName("")
    elementIP.current.focus()
}

function getValue(){
    console.log(elementIP)
    SetFirstName(elementIP.current.value)
    
}

useEffect(()=>{

prevElementIP.current = elementIP.current.value
prevCounter.current = counter

},[firstName,counter])

console.log("I am calling")
return(
    <>
    NameChanged:{firstName}<br/>
    Counter:{counter}<br/>
    PreviousNameChanged:{prevElementIP.current}
    PreviousCounter:{prevCounter.current}
    <input ref={elementIP} type="text" value={firstName} onChange={getValue}/>
    <button onClick={changeName}>Click Me!</button>
    <button onClick={(e)=>changeName(e)}>Get Value</button>
    <button onClick={()=>SetCounter(Math.ceil(Math.random()*100))}>Get Counter</button>
    </>
)


}

export default Ref;
