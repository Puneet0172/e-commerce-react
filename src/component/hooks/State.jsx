import { useState } from "react"

function State (){

const [firstName,SetFirstName]=useState("");// firstName is initail stage of variable & setFirstName
// will be updated value of varibale.

function changeName(){

    /*use state basically use to update the variable or change the state of object, array or 
     variable. 
     The Hook takes an initial state value as an argument and returns an updated 
     state value whenever the setter function is called.
     */
    SetFirstName("Puneet")
}

function getValue(event){
    console.log(event)
    // SetFirstName((previous)=> previous) we can get the previous value of state.
    SetFirstName(event.target.value)
    
}

console.log("I am calling")
return(
    <>
    NameChanged:{firstName}
    <input type="text" value={firstName} onChange={(e)=>getValue(e)}/>
    <button onClick={changeName}>Click Me!</button>
    <button onClick={(e)=>changeName(e)}>Get Value</button>
    </>
)


}

export default State;
