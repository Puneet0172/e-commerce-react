import { useEffect, useState } from "react";

function Effect(){

    /*React lifecycle hook that calls a function after the component is mounted. 
    Use useLifecycles if you need both a mount and unmount function.*/

    /* it covers lifecycle of class component 

    1. componentDidMount // when page is printing the tags or loading (ngOnInit angular)
    2. componentDidUpdate // any update capture my this life cycle (ngOnChange angular)
    3. componentWillUnmount // when page is destroyed (ngOnDestroyed angular)

    // useEffect hooks take first arg as callBack function means function which return something


    NOte: ALways remeber which you setState directly on button or inside return function
    then it will go to infinite loop. if you want to setState in return function always use
    call back function 
    e.g => <button onClick={()=>SetRandomNumber(Math.random())}>Random Number</button>
    <button onClick={SetRandomNumber(Math.random())}>Random Number</button> it will go infinite
    */

    const [date,SetDate]=useState(new Date().toString())
    const [randomNumber,SetRandomNumber]=useState(Math.random())
   

    useEffect(()=>{//any change into state or anything change this method will call Ai call
        console.log("I run every time this component re-renders") 

        return()=>{
            console.log("I am calling when this function component is destroyed or unmounted")
        };
        
    },[randomNumber]) // if it is empty then it is called only once when page is mounted ngOnInit(),
    // if we don't give any empty array then it will work as ngOnChange()
// and if we pass any dependency inside this then it will call only when that paricular state will change


    return(
        <>
       Date: {date}
       Number: {randomNumber}
       {/* showUseEffect: {date} */}
        <button onClick={()=>SetDate(new Date().toString())}>Date</button> 
        <button onClick={()=>SetRandomNumber(Math.random())}>Random Number</button>
        </>
    )
}
export default Effect;