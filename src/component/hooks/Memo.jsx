import { useState,useMemo } from "react";
function Memo() {

    /*
    The React useMemo Hook returns a memoized value. 
    Think of memoization as caching a value so that it does not need to be recalculated
    */

    const [firstName, SetFirstName] = useState("");
    const [counter, SetCounter] = useState(0);
    // const result = factorial(counter) heavy function
    const result = useMemo(()=>{
return factorial(counter);
    },[counter])


    function factorial(n) {// if you have some heavy function  we can add into cache or 
        // memorized so we cannot do a recalculate
        let i=0;
        while(i<200000000) i++;
        if (n < 0) {
            return -1;
        }
        if (n === 0) {
            return 1
        }
        return n * factorial(n - 1)
    }

    return (
        <>
            Factorial of {counter} is: {result}
            <input type="text" value={firstName} onChange={(e) => SetFirstName(e.target.value)}></input>
            Counter: {counter}
            <button onClick={() => SetCounter(counter + 1)}>Countter</button>
        </>
    )
}
export default Memo;