import Multiselect from "multiselect-react-dropdown";
import { useState } from "react"
import { useDispatch } from "react-redux";
import { addPizza } from "../pizzaStoreActions/pizzaOrdersSlice";
import DisplayPizzaRedux from "./DisplayPizzaRedux";


export default function PizzaShopRedux(){

    const toppingOptions=[{name:"red-onion"},{name:"green-pepper"},{name:"panner"},{name:"capsicun"}]
    const pizzaSize = ["small","medium","large","x-large"]
    const [size,Setsize] = useState("");
    const [quantity,SetQuantity] = useState(0);
    const [topping,SetTopping] = useState([]);

    //save the data into store
    const dispatch = useDispatch()

    const orderPizza = ()=>{
        const payload = {
            size,
            quantity,
            topping
        }
        dispatch(addPizza(payload))
        console.log("pizza ordered ", payload)
    }
    const onSelect= (selectedList, selectedItem) =>{
        SetTopping(selectedList);
    }

    return(
        <>
        Pizza Size:<select  value={size} onChange={(e)=>Setsize(e.target.value)}>
            {pizzaSize.map((v,index)=>(
                <option key={index}>{v}</option>
            ))}
            
            </select><br/>
        Pizza Quantity:<input type="text" value={quantity} onChange={(e)=>SetQuantity(e.target.value)}></input><br/>
        Topping: <Multiselect options={toppingOptions} displayValue="name"
                    selectedValues={(e)=>e.target.value}
                    onSelect={onSelect}>
            
        </Multiselect>
        
        <button onClick={orderPizza}>Order pizza</button>
        <DisplayPizzaRedux/>
        </>
    )
}