import { createSlice, nanoid } from "@reduxjs/toolkit";

const initialState  = {
    pizzas: [{
        id:1, 
        pizza:""}]
}

export const pizzaOrdersSlice = createSlice({
    name:"pizzaOrders",
    initialState,
    reducers:{
        addPizza:(state,action)=>{
            const pizza ={
                id:nanoid(),
                pizza:action.payload,
                // quantity:action.payload.quantity,
                // topping: action.payload.topping
            }
           state?.pizzas?.push(pizza);
        },
        removePizza:(state,action)=>{
            state.pizzas= state.pizzas.filter((piz)=>piz.id !==action.payload)
        },
    }
})

export const {addPizza,removePizza} = pizzaOrdersSlice.actions
 export default pizzaOrdersSlice;