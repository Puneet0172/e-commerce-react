import { configureStore } from "@reduxjs/toolkit";
import pizzaOrdersSlice  from "../pizzaStoreActions/pizzaOrdersSlice";


export const pizzaStore = configureStore({
    reducer: pizzaOrdersSlice.reducer
        
    
})