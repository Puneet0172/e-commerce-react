import { useState } from "react";
import Addpizza from "./Addpizza";
import DisplayPizza from "./DisplayPizza";

export default function Pizzashop(){

    const [pizzaList,setPizzaList] = useState([]); 

    const getPizzaOrder=(pizzaName,pizzaCount)=>{

     setPizzaList([...pizzaList,{pizzaName:pizzaName, pizzaCount:pizzaCount}])

    }

    const deletePizzaOrder=(pizzaName)=>{

        setPizzaList(pizzaList.filter(v=> v.pizzaName !== pizzaName))
   
       }

    return(<>
    <div className="row container">
    <Addpizza addPizza={getPizzaOrder}></Addpizza>
    <DisplayPizza deletePizaa={deletePizzaOrder} pizzaList={pizzaList}></DisplayPizza>
    </div>  
    </>)
}