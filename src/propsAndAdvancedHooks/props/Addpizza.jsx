import { useState } from "react"

export default function Addpizza({addPizza}){

    const [pizzaName,SetPizzaName]= useState("")
    const [pizzaCount,SetPizzaCount]= useState(0)

    function getValue(e,pizzaName= true){
        let values= e.target.value
        pizzaName ? SetPizzaName(values) : SetPizzaCount(values)
    }

    const orderPizza = ()=>{
        addPizza(pizzaName,pizzaCount)
        SetPizzaName("")
        SetPizzaCount(0);
    }


    return(<>


   <div className="row col-md-12 align-items-center">
    <div className="col-sm-3 my-1">
      <label className="sr-only" >Name</label>
      <input type="text" value={pizzaName} onChange={(e)=>getValue(e,true)} placeholder="pizza Name"></input>
    </div>
    
    <div className="col-sm-3 my-1">
      <label className="sr-only">How Many</label>
      <input type="number" value={pizzaCount} onChange={(e)=>getValue(e,false)} placeholder="how many"></input>
    </div>
    <div className="col-sm-2 my-1">
      <button type="submit" disabled={(pizzaName==="" && pizzaCount===0)} onClick={orderPizza} className="btn btn-primary">ADD</button>
    </div>
</div>
   
</>)
}