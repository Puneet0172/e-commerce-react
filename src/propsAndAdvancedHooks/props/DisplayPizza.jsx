export default function DisplayPizza({pizzaList,deletePizaa}){

    return (<>

{pizzaList.length === 0 ? <div>No Pizza Order</div> :

    <table className="table">
  <thead>
    <tr>
      <th scope="col">So.No</th>
      <th scope="col">Pizza Name</th>
      <th scope="col">Counts</th>
    </tr>
  </thead>
  <tbody>
    {pizzaList.map((v,index)=>(
      <tr key={index}>
      <th scope="row">{index+1}</th>
      <td>{v.pizzaName}</td>
      <td>{v.pizzaCount}</td>
      <td><button onClick={()=>deletePizaa(v.pizzaName)}>Delete</button></td>
    </tr>
    ))}
    
  </tbody>
</table>}
    </>)
}