import { useState } from "react"
import usecurrency from "./useCurrency"
import { CommonContext } from "../../store/CommonContext";
import CurrencyBox from "./currencyBox";

export default function CurrencyDisplaymainpage(){

const [amount,SetAmount]=useState(0);
const [convertedAmount,SetConvertedAmount]=useState(0);
const [from,SetFrom]=useState("usd");
const [to,SetTo]=useState("inr");
const apiData = usecurrency(from)
const options = Object.keys(apiData);

const convert = ()=>{
    SetConvertedAmount(amount * apiData[to]);
}




const combinedObject ={
    onAmountChange:(am)=>  SetAmount(am),
    onFromChange:(am)=>  SetFrom(am),
    onToChange:(am)=>  SetTo(am),
    onCovertClick:convert,
    amount,convertedAmount,from,to,options// if key and values are same
    }
    console.log("combinedObject ", combinedObject);

    return<>
    <CommonContext.Provider value={combinedObject}>
        <CurrencyBox></CurrencyBox>
    </CommonContext.Provider>
    
    </>
}