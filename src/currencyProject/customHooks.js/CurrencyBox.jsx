import { useContext } from "react"
import { CommonContext } from "../../store/CommonContext"

export default function CurrencyBox(){

    const sharedData = useContext(CommonContext)

    return<>
    
    
    <input value={sharedData.amount} type="text" onChange={(e)=>sharedData.onAmountChange(Number(e.target.value))}/>
    From
    <select onChange={(e)=>sharedData.onFromChange && sharedData.onFromChange(e.target.value)} >
        {sharedData.options.map(v=>(
            <option key={v}>{v}</option>
        ))}
        
    </select>
    To <select onChange={(e)=>sharedData.onToChange && sharedData.onToChange(e.target.value)} >
        {sharedData.options.map(v=>(
            <option key={v}>{v}</option>
        ))}
        
    </select>
    <button onClick={sharedData.onCovertClick}>Convert {sharedData.from} to {sharedData.to} </button>
     <br/>
    converted Amount: {sharedData.convertedAmount}
   
    </>
}